# Monitoring - Log Agent

[[_TOC_]]

## Description

A [Promtail](https://grafana.com/docs/loki/latest/clients/promtail/) instance to be run on all nomad nodes allowing the collection of
service logs to be forwarded to [Loki](https://grafana.com/oss/loki/) within [CarbonCollins - Cloud](https://gitlab.com/carboncollins-cloud).
