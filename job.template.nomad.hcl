job "log-agent" {
  name = "Log Agent (Promtail)"
  type = "system"
  region = "se"
  datacenters = ["soc"]
  namespace = "c3-monitoring"

  priority = 60

  group "agent" {
    count = 1

    volume "nomad-allocs" {
      type = "host"
      read_only = true
      source = "nomad-allocs"
    }

    volume "consul-agent-http" {
      type = "host"
      read_only = true
      source = "consul-agent-http"
    }

    network {
      mode = "bridge"

      port "http" {
        static = 3200
      }
    }

    service {
      name = "promtail"
      port = "http"
      task = "promtail"

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "loki"
              local_bind_port = 3100
            }
          }
        }
      }

      check {
        name     = "Promtail HTTP"
        type     = "http"
        path     = "/targets"
        interval = "5s"
        timeout  = "2s"

        check_restart {
          limit           = 2
          grace           = "60s"
          ignore_warnings = false
        }
      }
    }

    ephemeral_disk {
      sticky  = true
    }

    task "promtail" {
      driver = "docker"

      env {
        TZ = "[[ .defaultTimezone ]]"
        HOSTNAME = "${attr.unique.hostname}"
      }

      template {
        data        = <<EOH
[[ fileContents "./config/promtail.yml.tpl" ]]
EOH
        destination = "local/promtail.yml"
      }

      config {
        image = "[[ .promtailImageName ]]"

        args = [
          "-config.file=/local/promtail.yml",
          "-config.expand-env=true",
          "-server.http-listen-port=${NOMAD_PORT_http}",
        ]
      }

      resources {
        cpu = 200
        memory = 50
      }

      volume_mount {
        volume = "nomad-allocs"
        destination = "/nomad-alloc"
        read_only = true
      }

      volume_mount {
        volume = "consul-agent-http"
        destination = "/run/consul-agent-http.sock"
        read_only = true
      }
    }
  }

  meta {
    gitSha      = "[[ .gitSha ]]"
    gitBranch   = "[[ .gitBranch ]]"
    pipelineId  = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId   = "[[ .projectId ]]"
    projectUrl  = "[[ .projectUrl ]]"
  }
}
