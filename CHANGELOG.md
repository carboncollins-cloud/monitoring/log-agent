# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [Unreleased]

### Changed
- Updated repo to conform to new soc datacenter

## [2022-05-15]

### Added
- Created repo

### Changed
- Made promtail use an ephemeral disk
