positions:
  filename: /local/positions.yml

clients:
  - url: http://${NOMAD_UPSTREAM_ADDR_loki}/loki/api/v1/push

scrape_configs:
- job_name: 'nomad-logs'
  consulagent_sd_configs:
    - server: 'unix:///run/consul-agent-http.sock'
      datacenter: soc
  relabel_configs:
    - source_labels: [__meta_consulagent_node]
      target_label: __host__
    - source_labels: [__meta_consulagent_service_metadata_external_source]
      target_label: source
      regex: (.*)
      replacement: '$1'
    - source_labels: [__meta_consulagent_service_id]
      regex: '_nomad-task-([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})-.*'
      target_label:  'task_id'
      replacement: '$1'
    - source_labels: [__meta_consulagent_tags]
      regex: ',(app|monitoring),'
      target_label:  'group'
      replacement:   '$1'
    - source_labels: [__meta_consulagent_service]
      target_label: job
    - source_labels: ['__meta_consulagent_node']
      regex:         '(.*)'
      target_label:  'instance'
      replacement:   '$1'
    - source_labels: [__meta_consulagent_service_id]
      regex: '_nomad-task-([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})-.*'
      target_label:  '__path__'
      replacement: '/nomad-alloc/$1/alloc/logs/*std*.{?,??}'
